#include <string>
#include "gtest/gtest.h"
#include "scrabble.hpp"

std::string testword1 = "CABBAGE";
std::string testword2 = "";
std::string testword3 = "QQQ";



TEST(testword1, StandartTest)
{
  ASSERT_EQ(scrabble(testword1), 14);
}

TEST(testword2, ZeroLettersTest)
{
  ASSERT_EQ(scrabble(testword2), 0);
}

TEST(testword3, SameLettersTest)
{
  ASSERT_EQ(scrabble(testword3), 10);
}

