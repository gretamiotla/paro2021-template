#include "day-of-year.hpp"
// TE

int dayOfYear(int month, int dayOfMonth, int year) {

    const int january   = 31;
    int february        = 28; // february depends on the year
    const int march     = 31;
    const int may       = 31;
    const int july      = 31;
    const int august    = 31;
    const int october   = 31;
    const int december  = 31;
    const int april     = 30;
    const int june      = 30;
    const int september = 30;
    const int november  = 30;

    if((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
        february = 29;
    }

    if(month == 2) {
        dayOfMonth += january;
    } else if(month == 3) {
        dayOfMonth += january + february;
    } else if(month == 4) {
        dayOfMonth += january + february + march;
    } else if(month == 5) {
        dayOfMonth += january + february + march + april;
    } else if(month == 6) {
        dayOfMonth += january + february + march + april + may;
    } else if(month == 7) {
        dayOfMonth += january + february + march + april + may + june;
    } else if(month == 8) {
        dayOfMonth += january + february + march + april + may + june + july;
    } else if(month == 9) {
        dayOfMonth +=
            january + february + march + april + may + june + july + august;
    } else if(month == 10) {
        dayOfMonth += january + february + march + april + may + june + july +
                      august + september;
    } else if(month == 11) {
        dayOfMonth += january + february + march + april + may + june + july +
                      august + september + october;
    } else if(month == 12) {
        dayOfMonth += january + february + march + april + may + june + july +
                      august + september + october + november;
    }
    return dayOfMonth;
}
