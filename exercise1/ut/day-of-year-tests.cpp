#include "gtest/gtest.h"
#include "day-of-year.hpp"

struct DayOfYearTestSuite {};

/*TEST(DayOfYearTestSuite, dummyTest)
{
  ASSERT_TRUE(false);
}*/

TEST(DayOfYearTestSuite, January1stIsFitstDayOfYear)
{
  ASSERT_EQ(dayOfYear(1, 1, 2020), 1);
}

TEST(DayOfYearTestSuite, March1stIs61DayOfLeapYear)
{
  ASSERT_EQ(dayOfYear(3, 1, 2020), 61);
}

TEST(DayOfYearTestSuite, Jan1stIs1DayOfLeapYear)
{
  ASSERT_EQ(dayOfYear(2, 1, 3), 32);
}
TEST(DayOfYearTestSuite, Feb1stIs60thDayOfNormalYear)
{
  ASSERT_EQ(dayOfYear(2, 29, 2010), 60);
}
TEST(DayOfYearTestSuite, Dec31stIs366thDayOfLeapYear)
{
  ASSERT_EQ(dayOfYear(12, 31, 2020), 366);
}
